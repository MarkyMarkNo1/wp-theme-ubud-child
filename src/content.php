<?php
/**
 * The default template for displaying content
 *
 * @package Ubud
 * @since Ubud 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<?php if (has_post_thumbnail() ) : ?>
    <div class="entry-thumbnail cf">

        <?php if ('square' == get_theme_mod( 'thumbnailformat' ) && '1-column' == get_theme_mod( 'grid' )) : ?>
            <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'ubud' ), the_title_attribute( 'echo=0' ) ) ); ?>"class="thumb-img">
              <?php if ( get_theme_mod( 'expand_thumbs' ) ) : ?>
                <span class="attachment-img-square-big size-img-square-big wp-post-image" style="background-image: url(<?php the_post_thumbnail_url('img-square-big'); ?>);"></span>
              <?php else : the_post_thumbnail('img-square-big'); endif; ?>
            </a>
        <?php elseif ('square' == get_theme_mod( 'thumbnailformat' )) : ?>
            <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'ubud' ), the_title_attribute( 'echo=0' ) ) ); ?>"class="thumb-img">
              <?php if ( get_theme_mod( 'expand_thumbs' ) ) : ?>
                <span class="attachment-img-square size-img-square wp-post-image" style="background-image: url(<?php the_post_thumbnail_url('img-square'); ?>);"></span>
              <?php else : the_post_thumbnail('img-square'); endif; ?>
            </a>
        <?php elseif ('portrait' == get_theme_mod( 'thumbnailformat' ) && '1-column' == get_theme_mod( 'grid' )) : ?>
            <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'ubud' ), the_title_attribute( 'echo=0' ) ) ); ?>"class="thumb-img">
              <?php if ( get_theme_mod( 'expand_thumbs' ) ) : ?>
                <span class="attachment-img-portrait-big size-img-portrait-big wp-post-image" style="background-image: url(<?php the_post_thumbnail_url('img-portrait-big'); ?>);"></span>
              <?php else : the_post_thumbnail('img-portrait-big'); endif; ?>
            </a>
        <?php elseif ('portrait' == get_theme_mod( 'thumbnailformat' )) : ?>
            <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'ubud' ), the_title_attribute( 'echo=0' ) ) ); ?>"class="thumb-img">
              <?php if ( get_theme_mod( 'expand_thumbs' ) ) : ?>
                <span class="attachment-img-portrait size-img-portrait wp-post-image" style="background-image: url(<?php the_post_thumbnail_url('img-portrait'); ?>);"></span>
              <?php else : the_post_thumbnail('img-portrait'); endif; ?>
            </a>
        <?php elseif ('landscape' == get_theme_mod( 'thumbnailformat' ) && '1-column' == get_theme_mod( 'grid' )) : ?>
            <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'ubud' ), the_title_attribute( 'echo=0' ) ) ); ?>" class="thumb-img">
              <?php if ( get_theme_mod( 'expand_thumbs' ) ) : ?>
                <span class="attachment-img-landscape-big size-img-landscape-big wp-post-image" style="background-image: url(<?php the_post_thumbnail_url('img-landscape-big'); ?>);"></span>
              <?php else : the_post_thumbnail('img-landscape-big'); endif; ?>
            </a>
        <?php elseif ('landscape' == get_theme_mod( 'thumbnailformat' )) : ?>
            <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'ubud' ), the_title_attribute( 'echo=0' ) ) ); ?>" class="thumb-img">
              <?php if ( get_theme_mod( 'expand_thumbs' ) ) : ?>
                <span class="attachment-img-landscape size-img-landscape wp-post-image" style="background-image: url(<?php the_post_thumbnail_url('img-landscape'); ?>);"></span>
              <?php else : the_post_thumbnail('img-landscape'); endif; ?>
            </a>
        <?php endif; ?>

        <div class="entry-header">
            <h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'ubud' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
            <div class="entry-content cf">
                <?php the_excerpt(); ?>
            </div><!-- end .entry-content -->
            <div class="entry-details">
                <div class="entry-date">
                    <a href="<?php the_permalink(); ?>"><?php echo get_the_date(); ?></a>
                    </div><!-- end .entry-date -->
                <?php if ( comments_open() ) : ?>
                <div class="entry-comments">
                    <?php comments_popup_link( '<span class="leave-reply">' . __( 'Leave a comment', 'ubud' ) . '</span>', __( 'comment 1', 'ubud' ), __( 'comments %', 'ubud' ) ); ?>
                </div><!-- end .entry-comments -->
                <?php endif; // comments_open() ?>
            </div><!-- end .entry-details -->
        </div><!-- end .entry-header -->

    </div><!-- end .entry-thumbnail -->

    <?php else : ?>

    <div class="entry-thumbnail cf">
        <?php if ('square' == get_theme_mod( 'thumbnailformat' )) : ?>
            <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'ubud' ), the_title_attribute( 'echo=0' ) ) ); ?>" class="thumb-img"><img src="<?php echo get_template_directory_uri(); ?>/images/default-square.png" alt="<?php the_title(); ?>" /></a>
        <?php elseif ('portrait' == get_theme_mod( 'thumbnailformat' )) : ?>
            <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'ubud' ), the_title_attribute( 'echo=0' ) ) ); ?>" class="thumb-img"><img src="<?php echo get_template_directory_uri(); ?>/images/default-portrait.png" alt="<?php the_title(); ?>" /></a>
        <?php elseif ('landscape' == get_theme_mod( 'thumbnailformat' )) : ?>
            <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'ubud' ), the_title_attribute( 'echo=0' ) ) ); ?>" class="thumb-img"><img src="<?php echo get_template_directory_uri(); ?>/images/default-landscape.png" alt="<?php the_title(); ?>" /></a>
        <?php endif; ?>

        <div class="entry-header">
            <h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'ubud' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
            <div class="entry-content cf">
                <?php the_excerpt(); ?>
            </div><!-- end .entry-content -->
            <div class="entry-details">
                <div class="entry-date">
                    <a href="<?php the_permalink(); ?>" class="entry-date"><?php echo get_the_date(); ?></a>
                    </div><!-- end .entry-date -->
                <?php if ( comments_open() ) : ?>
                <div class="entry-comments">
                    <?php comments_popup_link( '<span class="leave-reply">' . __( 'comment 0', 'ubud' ) . '</span>', __( 'comment 1', 'ubud' ), __( 'comments %', 'ubud' ) ); ?>
                </div><!-- end .entry-comments -->
                <?php endif; // comments_open() ?>
            </div><!-- end .entry-details -->
        </div><!-- end .entry-header -->
    </div><!-- end .entry-thumbnail -->
    <?php endif; ?>

</article><!-- end post -<?php the_ID(); ?> -->
