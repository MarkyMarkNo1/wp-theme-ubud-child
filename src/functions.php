<?php
/**
 * Custom functions
 *
 * @package ubud
 *
 */


/**
 * Enqueue scripts and styles for the frontend
 */
function ubud_custom_scripts() {
  /* Load our parent theme's main stylesheet */
  wp_enqueue_style( 'ubud-parent-style', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'ubud_custom_scripts' );



/**
 * Implement Theme Customizer additions and adjustments.
 *
 * Taken from Ponsonby theme
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 *
 * @since Ubud 1.0
 */
function ubud_custom_customize_register( $wp_customize ) {

  $wp_customize->add_setting( 'expand_thumbs', array(
    'default' => '',
    'sanitize_callback' => 'ubud_custom_sanitize_checkbox',
  ) );

  $wp_customize->add_control( 'expand_thumbs', array(
    'label'   => __( 'Expand inappropriate thumbnails', 'ubud' ),
    'section' => 'ubud_themeoptions',
    'type'    => 'checkbox',
  ) );

  $wp_customize->add_setting( 'hide_singlethumb', array(
    'default' => '',
    'sanitize_callback' => 'ubud_custom_sanitize_checkbox',
  ) );

  $wp_customize->add_control( 'hide_singlethumb', array(
    'label'   => __( 'Hide Featured Image on single posts', 'ubud' ),
    'section' => 'ubud_themeoptions',
    'type'    => 'checkbox',
    'priority'  => 11,
  ) );

}
add_action( 'customize_register', 'ubud_custom_customize_register' );



/**
 * Sanitize Checkboxes.
 *
 * Taken from Ponsonby theme
 *
 */
function ubud_custom_sanitize_checkbox( $input ) {
  if ( 1 == $input ) {
    return true;
  } else {
    return false;
  }
}



/*
 * Utilizing body_class filter
 */
function ubud_custom_adjust_body_class( $classes ) {

  /*
   * Additional class intended to enable a specific layout for single post views
   * without a featured image
   */
  if ( is_singular( 'post' ) && ( '' == get_the_post_thumbnail() || get_theme_mod( 'hide_singlethumb' ) ) ) {
    $classes[] = 'single-post-no-featured-image';
  }

  return $classes;
}
add_filter( 'body_class','ubud_custom_adjust_body_class' );



/**
 * Unveil Base64 encoded mailto-links
 *
 * Unfortunately, data attributes might get stripped from html output, which
 * forbids utilizing them like *[data-encoded-email]. Instead we'll use
 * a.ubud-custom-data-encoded-email with Base64 decoded email address as text.
 *
 * Base64 decoding: http://scotch.io/quick-tips/js/how-to-encode-and-decode-strings-with-base64-in-javascript
 * #ToDo: Polyfill! See http://caniuse.com/#feat=atob-btoa
 */
function ubud_custom_unveil_mailto () {
?>
<!-- ubud_custom_unveil_mailto -->
<script>
if (window.jQuery) jQuery(function($){
  $('a.ubud-custom-data-encoded-email').each(function(){
    var $this = $(this),
      email = $this.text();
    try {
        email = atob(email);
    } catch (e) {
      console.log('Seems your browser doesn\'t support Base64 decoding ;/');
    }
    $this.text(email);
    $this.attr('href', 'mailto:' + email + '?subject=<?php echo $_SERVER['SERVER_NAME']; ?>');
  });
});
</script>
<!-- end ubud_custom_unveil_mailto -->
<?php
}
add_action( 'wp_footer', 'ubud_custom_unveil_mailto' );



