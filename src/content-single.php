<?php
/**
 * The template for displaying content in the single.php template
 *
 * @package Ubud
 * @since Ubud 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?>>

	<?php if ( '' != get_the_post_thumbnail() && ! post_password_required() && ! get_theme_mod('hide_singlethumb') ) : ?>
	<div class="entry-thumbnail">
		<?php the_post_thumbnail(); ?>
	</div><!-- end .entry-thumbnail -->
	<?php endif; ?>

	<header class="entry-header">
		<nav id="nav-single" class="clearfix">
			<div class="nav-next"><?php next_post_link( '%link', ( '<span>' . __( 'Next', 'ubud' ) . '</span>' ) ); ?></div>
			<div class="nav-previous"><?php previous_post_link( '%link', ( '<span>' . __( 'Previous', 'ubud' ) . '</span>' ) ); ?></div>
		</nav><!-- #nav-single -->
		<h1 class="entry-title"><?php the_title(); ?></a></h1>
		<div class="entry-details">
			<div class="entry-author">
				<?php
					printf( __( 'by <a href="%1$s" title="%2$s">%3$s</a>', 'ubud' ),
					esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
					sprintf( esc_attr__( 'All posts by %s', 'ubud' ), get_the_author() ),
					get_the_author() );
				?>
			</div><!-- end .entry-author -->
			<div class="entry-date">
				<a href="<?php the_permalink(); ?>"><?php echo esc_html( get_the_date() ); ?></a>
			</div><!-- end .entry-date -->
			<?php if ( comments_open() ) : ?>
				<div class="entry-comments">
				<?php comments_popup_link( '<span class="leave-reply">' . __( 'comment 0', 'ubud' ) . '</span>', __( 'comment 1', 'ubud' ), __( 'comments %', 'ubud' ) ); ?>
				</div><!-- end .entry-comments -->
			<?php endif; // comments_open() ?>
			<?php edit_post_link( __( 'Edit', 'ubud' ), '<div class="entry-edit">', '</div>' ); ?>
			<div class="entry-cats"><?php the_category(', '); ?></div>
			<?php $tags_list = get_the_tag_list();
			if ( $tags_list ): ?>
			<div class="entry-tags"><?php the_tags('<ul><li>',', ','</li></ul>'); ?></div>
			<?php endif; // get_the_tag_list() ?>
			<?php if ( get_theme_mod( 'sharebtn' ) ) : ?>
				<?php get_template_part( 'share'); ?>
			<?php endif; ?>
		</div><!--end .entry-details -->
	</header><!--end .entry-header -->

	<div class="entry-content cf">
		<?php the_content(); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'ubud' ), 'after' => '</div>' ) ); ?>
	</div><!-- end .entry-content -->

		<?php if ( get_the_author_meta( 'description' )) : // If a user filled out their author bio ?>
		<div class="author-wrap cf">
			<div class="author-info">
				<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'ubud_author_bio_avatar_size', 70 ) ); ?>
				<h6><?php printf( __( 'Posted by %s', 'ubud' ), "<a href='" .  esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )) . "' title='" . esc_attr( get_the_author() ) . "' rel='author'>" . get_the_author() . "</a>" ); ?></h6 >
				<p class="author-description"><?php the_author_meta( 'description' ); ?></p>
			</div><!-- end .author-info -->
		</div><!-- end .author-wrap -->
	<?php endif; ?>

</article><!-- end .post-<?php the_ID(); ?> -->
