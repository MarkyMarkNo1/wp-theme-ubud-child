'use strict';

module.exports = function (grunt) {

  // Configurable paths
  var config = {
    app: 'ubud-custom',
    src: 'src',
    tmp: '.tmp',
    dist: 'dist'
  };

  // Display the elapsed execution time of grunt tasks
  // https://www.npmjs.com/package/time-grunt
  require('time-grunt')(grunt);

  // JIT plugin loader for Grunt
  // https://www.npmjs.com/package/jit-grunt
  require('jit-grunt')(grunt);

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    config: config,

    // Grunt Task for Installing Bower Dependencies
    // https://npmjs.org/package/grunt-bower-install-simple
    'bower-install-simple': {
      all: {}
    },

    // Validate files with ESLint
    // https://www.npmjs.com/package/grunt-eslint
    eslint: {
      all: [
        'Gruntfile.js'
      ]
    },

    // Clean files and folders
    // https://www.npmjs.com/package/grunt-contrib-clean
    clean: {
      tmp: {
        files: [{
          dot: true,
          src: [
            '<%= config.tmp %>'
          ]
        }]
      },
      dist: {
        files: [{
          dot: true,
          src: [
            '<%= config.dist %>/*',
            '!<%= config.dist %>/.git*'
          ]
        }]
      }
    },

    // Run predefined tasks whenever watched file patterns are added, changed or
    // deleted
    // https://www.npmjs.com/package/grunt-contrib-watch
    watch: {
      less: {
        files: [
          '<%= config.src %>/**/*.less'
        ],
        tasks: ['less']
      }
    },

    // Copy files and folders
    // https://www.npmjs.com/package/grunt-contrib-copy
    copy: {
      src: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.src %>',
          dest: '<%= config.tmp %>/<%= config.app %>',
          src: [ '**' ]
        }]
      }
    },

    // Compile LESS files to CSS
    // https://www.npmjs.com/package/grunt-contrib-less
    less: {
      style: {
        files: {
          '<%= config.tmp %>/<%= config.app %>/style.css': '<%= config.src %>/style.less'
        }
      }
    },

    // Compress files and folders
    // https://www.npmjs.org/package/grunt-contrib-compress
    compress: {
      dist: {
        options: {
          archive: '<%= config.dist %>/<%= config.app %>.zip',
          mode: 'zip'
        },
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.tmp %>',
          src: [ '**' ],
          dest: ''
        }]
      }
    },

    // Automatic desktop notifications for Grunt errors and warnings
    // https://www.npmjs.com/package/grunt-notify
    notify: {
      build: {
        options: {
          title: 'Build successful',
          message: '\'build\' task done, without errors.'
        }
      }
    }

  });


  grunt.registerTask('default', [
    'build'
  ]);

  grunt.registerTask('serve', [
    'clean:tmp',
    'bower-install-simple',
    'watch'
  ]);

  grunt.registerTask('build', [
    'newer:eslint',
    'clean',
    'bower-install-simple',
    'copy',
    'less',
    'compress',
    'notify:build'
  ]);

};
