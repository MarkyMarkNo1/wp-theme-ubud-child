# "Ubud" child theme for WordPress #

WordPress child theme for "Ubud" by [Elmastudio](http://www.elmastudio.de/).

![screenshot.png](//bytebucket.org/MarkyMarkNo1/wp-theme-ubud-child/raw/c7d540ff136e860503f99cac651d5ced614b45c5/ubud-custom/screenshot.png)

## Parent theme ##

* ["Ubud" homepage](http://www.elmastudio.de/en/wordpress-en/minimal-photography-wordpress-theme-ubud/)
* ["Ubud" documentation](http://www.elmastudio.de/en/themes/docs/ubud/)
* ["Ubud" demo](http://themes.elmastudio.de/ubud/)

## Child theme enhancements ##

* Theme option "Expand inappropriate thumbnails"
* Theme option "Hide Featured Image on single posts"
* Featured Attachment: treat an image with caption as first element of a post like a Featured Image
* Unveils Base64 encoded mailto-links (function ubud_custom_unveil_mailto)
* Layout improvements

## About WordPress ##

* [WordPress Codex](http://codex.wordpress.org/Child_Themes)
* [WordPress.org](http://wordpress.org/)
